#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>


int main(int argc, char **argv) {
    int fd;
    int buff[3];

// borra FIFO por si existía previamente
    unlink("/tmp/FIFO");
    /* crea el FIFO */
   
    if (mkfifo("/tmp/FIFO", 0600)<0) {
        perror("No puede crearse el FIFO");
        return(1);
    }
    /* Abre el FIFO */
    if ((fd=open("/tmp/FIFO", O_RDONLY))<0) {
        perror("No puede abrirse el FIFO");
        return(1);
    }
    while(1){
    	if(read(fd,buff,sizeof(buff))>=sizeof(buff))
    		printf("punto jugador %d  %d-%d\n", buff[0], buff[1],buff[2]);
    }
  
  }   

