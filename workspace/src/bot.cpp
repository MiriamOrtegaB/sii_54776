
#include <stdio.h>
#include <unistd.h>
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include "DatosMemCompartida.h"

int main(int argc,char* argv[])
{
	DatosMemCompartida* com;

	int fd = open("/tmp/tenis_bot",O_RDWR);
	
	com = (DatosMemCompartida *) mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	close(fd);


	while(1){
		usleep(25000);
		if(com->esfera.centro.y < (com->raqueta.y1+com->raqueta.y2)/2) 
		 com->accion = -1; //se mueva hacia abaajo
		else if(com->esfera.centro.y > (com->raqueta.y1+com->raqueta.y2)/2) 
			com->accion = 1; //se mueva hacia arriba
		else
			com->accion = 0;//no se mueeve
	}
	return 0;   
}
