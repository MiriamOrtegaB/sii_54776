# Changelog
Aqui se describen todos los cambios realizados de las practicas de laboratorio de SII de MIRIAM ORTEGA BUSTOS (54776). 

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.0] - 2021-11-22
### Added
- Creacion de logger
- Creación de bot
- Añadido final del juego

## [1.2.0] - 2021-10-26
### Added
- Disminución del tamaño de la pelota según avanza el juego.
- Añadido el movimiento de las raquetas y de la pelota.
- Añadido el readme.

### Changed
- Añadido con un comentario la autora de Esfera.cpp.

## [1.1.0] - 2021-10-24
### Added
- Añadido el changelog.

### Changed
- Añadido con un comentario la autora de Esfera.cpp.
