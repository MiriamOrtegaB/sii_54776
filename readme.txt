oOoOOoOOo o.OOoOoo o.     O ooOoOOo .oOOOo.  
    o      O       Oo     o    O    o     o  
    o      o       O O    O    o    O.       
    O      ooOO    O  o   o    O     `OOoo.  
    o      O       O   o  O    o          `O 
    O      o       o    O O    O           o 
    O      O       o     Oo    O    O.    .O 
    o'    ooOooOoO O     `o ooOOoOo  `oooO'


FUNCIONAMIENTO DEL JUEGO:
Dos jugadores manejan dos raquetas para darle a una pelota hasta conseguir que el adversario pierda.
Un jugador pierde cuando no le da a la pelota con la raqueta.   

CONTROLES:
W: movimiento vertical hacia arriba de la primera raqueta.
S: movimiento vertical hacia abajo de la primera raqueta.

O: movimiento vertical hacia arriba de la segunda raqueta.
L: movimiento vertical hacia abajo de la segunda raqueta.

